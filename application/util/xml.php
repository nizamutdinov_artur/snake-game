<?php


class XML
{
    static function DownloadXml ($URI)
    {
        if (($response_xml_data = @file_get_contents($URI)) === false) {
            echo "Error fetching XML\n";
        } else {
            libxml_use_internal_errors(true);

            $data = simplexml_load_string($response_xml_data);
            if (!$data) {
                echo "Error loading XML\n";
                foreach (libxml_get_errors() as $error) {
                    echo "\t", $error->message;
                }
            } else {
                return $data;
            }
        }
    }


    static function Xml2Array ( $xmlObject )
    {
        $out = array();

        foreach ((array)$xmlObject as $index => $node)
        {
            $out[$index] = (is_object($node)) ? XML::Xml2Array($node) : $node;
        }

        return $out;
    }
}