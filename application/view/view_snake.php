
    <div class="formBox">
        <form>
            <input type="button" id="easyButton" value="Easy">
            <input type="button" id="normalButton" value="Normal">
            <input type="button" id="hardButton" value="Hard">
            <input type="button" id="veryHardButton" value="Very Hard">
        </form>
    </div>
    <div id="canvasBox">

        <canvas id="display">Please update you browser</canvas>

        <script type="text/javascript" src="js/main.js"></script>

        <script type="text/javascript" src="js/game/snake_game.js"></script>


    </div>