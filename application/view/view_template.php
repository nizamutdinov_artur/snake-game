<!DOCTYPE html>
<html lang="ru">
<head>

    <Title><?=$data['Title']?></Title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/icon.ico"/>

    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Template CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- Head -->
    <?php
    if (file_exists($this->head))
    { include_once $this->head;}?>
</head>

<body>
    <!-- Navbar -->
    <?php
    if (file_exists($this->navbar))
    { include_once $this->navbar;}?>

<div class="content">

    <!-- Content -->
    <?php
    if(file_exists($this->content))
    { include $this->content;} ?>


</div>

<!-- Footer -->
<?php
if (file_exists($this->footer))
{ include_once $this->footer;}?>

</body>

</html>