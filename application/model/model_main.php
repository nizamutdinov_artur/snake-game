<?php

Class Model_Main extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get_Data()
    {
        $this->data["Title"] = "Home";
        $this->data['CurrentPage'] = 'Main';

        return $this->data;
    }

}
