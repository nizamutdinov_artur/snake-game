<?php

Class Model_TowerDefense extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get_Data()
    {
        $this->data["Title"] = "TD game";
        $this->data['CurrentPage'] = 'TD';

        return $this->data;
    }

}
