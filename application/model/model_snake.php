<?php

Class Model_Snake extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get_Data()
    {
        $this->data["Title"] = "Snake game";
        $this->data['CurrentPage'] = 'Snake';

        return $this->data;
    }

}
