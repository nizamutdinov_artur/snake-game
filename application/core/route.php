<?php

/*
    Класс-маршрутизатор для определения запрашиваемой страницы.
    > цепляет классы контроллеров и моделей;
    > создает экземпляры контролеров страниц и вызывает действия этих контроллеров.
*/
class route
{
    /**
     * @var array
     */
    private $directory;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $action;
    /**
     * @var null
     */
    private $args;
    /**
     *
     */
    public function __construct()
    {
        $this->directory = array(
            "head"=>"application/head/",
            "model"=>"application/model/",
            "controller"=>"application/controller/",
            "view"=>"application/view/"
        );

        // Default page
        $this->name = 'main';
        $this->action = 'Default_action';
        $this->args = null;

        $URI = $_SERVER['REQUEST_URI'];

        $split = explode('?', $URI);
        $commandNoArgs = $split[0];

        $command = explode('/', $commandNoArgs);

        $this->args= $this->GetArgsFromURI($URI);

        $this->StartController($this->name, $this->action, $this->args, $command);
    }


    private function GetArgsFromURI ($URI)
    {
        $args = NULL;

        $RegexPattern = "/[\\?\\&]([\\w\\W]+?)\\=([\\w\\d]+)/";

        preg_match_all($RegexPattern, $URI, $matches);

        for ($i = 0; $i < count($matches[1]); $i++)
        {
        $args[$matches[1][$i]] = $matches[2][$i];
        }
        return $args;
    }

    /**
     * @param $command
     */
    private function StartController($name, $action, $args,$command)
    {


        if  ($command[1] != "index.php")
        if (!empty($command[1]))
        {
            $name = $command[1];

            if (!empty($command[2]))
            {
                $action = $command[2];
            }
        }

        // Assign controller
        $controller_name = "controller_".$name;

        $controller_file = strtolower($controller_name).'.php';
        $controller_path = $this->directory['controller'].$controller_file;

        if(file_exists($controller_path))
        {
            include $controller_path;
            $controller = new $controller_name($name);

        }
        else
        {
            $this->ErrorPage404();
        }


        if(method_exists($controller,  $action))
        {
            $controller-> $action( $args);
        }
    }

    /**
     *
     */
   private function ErrorPage404()
    {
        echo "<h1>Error 404 this page doesn't exist</h1>";
        echo "<br>";
        echo  '<a href="/">Back to main page</a>';

        include 'application/view/view_404.php';
    }

}
