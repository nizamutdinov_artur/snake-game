<?php

Class Model
{
    protected $data;

    public function __construct()
    {
        $this->data = array();
    }

    public function Get_Data()
    {
        return $this->data;
    }

}