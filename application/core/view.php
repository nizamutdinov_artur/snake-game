<?php

Class View
{
    public $head;
    public $content;
    public $navbar;
    public $footer;

    public function GeneratePage( $data)
    {
        include 'application/view/view_template.php';
    }
}