    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!-- Form validation CSS -->
    <link rel="stylesheet" href="/css/formValidation.css"/>
    <!-- Bootstrap core Javascript -->
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <!-- Form validation Javascript -->
    <script type="text/javascript" src="/js/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="/js/formvalidation/framework/bootstrap.js"></script>

    <script type="text/javascript" src="/js/formvalidation/language/ru_RU.js"></script>

