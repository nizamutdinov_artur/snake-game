<?php

class Config
{

    private static $mysql = array(
        'host' => '',
        'username' => '',
        'password' => '',
        'dbname' => ''
    );

    /**
     * @return array
     */
    public static function GetMysql()
    {
        return self::$mysql;
    }

}
