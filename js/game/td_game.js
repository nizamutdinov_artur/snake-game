
var debugInfo = 0;


var SCREEN_BOUNDS = new Rectangle(0, 0, 500, 500);


function Point (x, y)
{
	this.X = x;
	this.Y = y;
}

function timestamp()
{
	return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
}

function Rectangle (x, y, width, height)
{
	this.X = x;
	this.Y = y;
	this.Width = width;
	this.Height = height;
}

function KeyboardState ()
{

	var _pressed = {};

	window.addEventListener(
		'keyup',
		function (event)
		{
			onKeyUp(event);
		},
		false);

	window.addEventListener(
		'keydown',
		function (event)
		{
			onKeyDown(event);
		},
		false);

	this.IsKeyDown = function(keyCode)
	{
		return _pressed[keyCode];
	};

	var onKeyDown = function(event) {
		_pressed[event.keyCode] = true;
	};

	var onKeyUp = function(event) {
		delete _pressed[event.keyCode];
	};

}

function MouseState()
{
	var _lButton;
	var _position = {};

	window.addEventListener(
		'mouseDown',
		function (event)
		{
			onMouseDown(event);
		},
		false);

	var onMouseDown = function (event){

	}
}

function getClickPosition(e) {
	var parentPosition = getPosition(e.currentTarget);
	var xPosition = e.clientX - parentPosition.x;
	var yPosition = e.clientY - parentPosition.y;

	return { X: xPosition, Y: yPosition };
}

function getPosition(element) {
	var xPosition = 0;
	var yPosition = 0;

	while (element) {
		xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
		yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
		element = element.offsetParent;
	}
	return { x: xPosition, y: yPosition };
}

var enums = {};

enums.Colors = {
	Head: "#FFFFFF",
	Body: "#FFFFFF",
	Apple: "#00FF00"
};

enums.Keys = {

	BACKSPACE: 8,
	TAB: 9,
	ENTER: 13,
	SHIFT: 16,
	CTRL: 17,
	ALT: 18,
	PAUSE: 19,
	CAPS_LOCK: 20,
	ESCAPE: 27,
	SPACE: 32,
	PAGE_UP: 33,
	PAGE_DOWN: 34,
	END: 35,
	HOME: 36,
	LEFT_ARROW: 37,
	UP_ARROW: 38,
	RIGHT_ARROW: 39,
	DOWN_ARROW: 40,
	INSERT: 45,
	DELETE: 46,
	KEY_0: 48,
	KEY_1: 49,
	KEY_2: 50,
	KEY_3: 51,
	KEY_4: 52,
	KEY_5: 53,
	KEY_6: 54,
	KEY_7: 55,
	KEY_8: 56,
	KEY_9: 57,
	KEY_A: 65,
	KEY_B: 66,
	KEY_C: 67,
	KEY_D: 68,
	KEY_E: 69,
	KEY_F: 70,
	KEY_G: 71,
	KEY_H: 72,
	KEY_I: 73,
	KEY_J: 74,
	KEY_K: 75,
	KEY_L: 76,
	KEY_M: 77,
	KEY_N: 78,
	KEY_O: 79,
	KEY_P: 80,
	KEY_Q: 81,
	KEY_R: 82,
	KEY_S: 83,
	KEY_T: 84,
	KEY_U: 85,
	KEY_V: 86,
	KEY_W: 87,
	KEY_X: 88,
	KEY_Y: 89,
	KEY_Z: 90,
	LEFT_META: 91,
	RIGHT_META: 92,
	SELECT: 93,
	NUMPAD_0: 96,
	NUMPAD_1: 97,
	NUMPAD_2: 98,
	NUMPAD_3: 99,
	NUMPAD_4: 100,
	NUMPAD_5: 101,
	NUMPAD_6: 102,
	NUMPAD_7: 103,
	NUMPAD_8: 104,
	NUMPAD_9: 105,
	MULTIPLY: 106,
	ADD: 107,
	SUBTRACT: 109,
	DECIMAL: 110,
	DIVIDE: 111,
	F1: 112,
	F2: 113,
	F3: 114,
	F4: 115,
	F5: 116,
	F6: 117,
	F7: 118,
	F8: 119,
	F9: 120,
	F10: 121,
	F11: 122,
	F12: 123,
	NUM_LOCK: 144,
	SCROLL_LOCK: 145,
	SEMICOLON: 186,
	EQUALS: 187,
	COMMA: 188,
	DASH: 189,
	PERIOD: 190,
	FORWARD_SLASH: 191,
	GRAVE_ACCENT: 192,
	OPEN_BRACKET: 219,
	BACK_SLASH: 220,
	CLOSE_BRACKET: 221,
	SINGLE_QUOTE: 222
};

enums.Direction = {
	Up: 1,
	Down: 2,
	Left: 3,
	Right: 4
};

enums.Fonts =
{
	Default: "10px 'Press Start 2P'",
	Big: "20px 'Press Start 2P'"
}

enums.GameStates = {
	Idle: 0,
	Game: 1,
	Dead: 2

}

function Graphics (ctx)
{
	var _ctx = ctx;

	_ctx.canvas.width = 500;
	_ctx.canvas.height = 500;

	this.DrawLine = function (p1, p2)
	{
		_ctx.beginPath();
		_ctx.moveTo(p1.X, p1.Y);
		_ctx.lineTo(p2.X, p2.Y);
		_ctx.stroke();
	};

	this.DrawText = function (pos, text, font, color)
	{
		_ctx.font = font;
		_ctx.fillStyle = color;
		_ctx.fillText(text, pos.X, pos.Y);
	};

	this.DrawRectangle = function (rectangle, color)
	{
		_ctx.fillStyle = color;
		_ctx.fillRect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
	};

	this.DrawImage = function (rectangle, image)
	{
		_ctx.drawImage(image, rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
	}

}

function SoundEffects ()
{
	var powerup = [];
	var die;

	var initialize = function()
	{
		for (var i = 1; i <= 5; i++)
		{
			powerup.push(new Audio("soundEffects/Jump" + i + ".wav"));
			powerup[i-1].volume = 0.5;
		}

		die = new Audio("soundEffects/die.wav");
		die.volume = 0.5;
	};

	initialize();

	this.Play = function ( name )
	{

		switch (name)
		{
			case 'powerup':
			{
				powerup[Helpers.GetRandomValue(0,4)].play();
				break;
			}
			case 'die':
			{
				die.play();
				break;
			}
		}
	}

}

function Game (display, cellSize)
{
	var _gameState = enums.GameStates.Idle;

	var graphics = new Graphics(display);

	var keyboardState = new KeyboardState();

	var soundEffects = new SoundEffects();

	var _cellSize = cellSize;

	var _score = 0;

	var Initialize = function ()
	{


		$('#display').on('touchstart click',
			function (event)
			{
				var pos = getClickPosition(event);

				snakeController.MobileInput(pos);
				if (Helpers.RectanglesIntersects(new Rectangle(pos.X, pos.Y, 1, 1), new Rectangle(140, 140, 221, 221)))
				{
					if (_gameState == enums.GameStates.Dead)
						Reset();

					_gameState = enums.GameStates.Game;
				}
			}
		);

	};

	Initialize();

	this.Update = function ()
	{
		switch (_gameState)
		{
			case enums.GameStates.Idle:
			{
				break;
			}

			case enums.GameStates.Game :
			{

				break;
			}
		}

	};


	this.Input = function ()
	{
	};

	var Reset = function ()
	{


		_score = 0;
	};

	this.Draw = function ()
	{
		switch (_gameState)
		{
			case enums.GameStates.Idle:
			{


				break;
			}

			case enums.GameStates.Game :
			{
					break;
			}
		}

	};
}

function MapController (rows, colums)
{
	var _mapArray = [rows];

	this.Update ( mousePos )
	{

	}
}

var Helpers = {};

Helpers.Create2DArray = function (rows)
	{
		var arr = [];

		for (var i = 0; i < rows; i++)
		{
			arr[i] = [];
		}

		return arr;
	};


Helpers.GetRandomValue = function (min, max)
{
	return Math.floor(Math.random() * max) + min;
};

Helpers.RectanglesIntersects = function (rect1, rect2)
{
	return !!(rect1.X < rect2.X + rect2.Width &&
	rect1.X + rect1.Width > rect2.X &&
	rect1.Y < rect2.Y + rect2.Height &&
	rect1.Height + rect1.Y > rect2.Y);
};

var CellSize = 20;

var NewGame = function ()
	{
		var displayElement = document.getElementById("display");
		var display = displayElement.getContext("2d");
		var now,
			dt = 0,
			last = timestamp();
			step = 1/8;
		var game = new Game(display, CellSize);


		function Frame() {
			now = timestamp();
			dt = dt + Math.min(1, (now - last) / 1000);
			while(dt > step) {
				dt = dt - step;
				Update();
			}
			last = now;
			Draw();
			requestAnimationFrame(Frame);
		}

		function Update()
		{
			game.Update();
		}

		function Draw ()
		{
			display.fillStyle = "#000000"
			display.fillRect(0, 0, 500, 500);
			game.Draw();
		}

		requestAnimationFrame(Frame);

	};

NewGame();
