var SCREEN_BOUNDS = new Rectangle(0, 0, 500, 500);


function Point (x, y)
{
	this.X = x;
	this.Y = y;
}

function timestamp()
{
	return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
}

function Rectangle (x, y, width, height)
{
	this.X = x;
	this.Y = y;
	this.Width = width;
	this.Height = height;
}




function KeyboardState ()
{
	var _pressed = {};

	this.IsKeyDown = function(keyCode)
	{
		return _pressed[keyCode];
	};

	this.OnKeyDown = function(event) {
		_pressed[event.keyCode] = true;
	};

	this.OnKeyUp = function(event) {
		delete _pressed[event.keyCode];
	};

}

function getClickPosition(e) {
	var parentPosition = getPosition(e.currentTarget);
	var xPosition = e.clientX - parentPosition.x;
	var yPosition = e.clientY - parentPosition.y;

	return { X: xPosition, Y: yPosition };
}

function getPosition(element) {
	var xPosition = 0;
	var yPosition = 0;

	while (element) {
		xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
		yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
		element = element.offsetParent;
	}
	return { x: xPosition, y: yPosition };
}

var enums = {};

enums.Colors = {
	Head: "#FFFFFF",
	Body: "#FFFFFF",
	Apple: "#00FF00"
};

enums.Keys = {

	BACKSPACE: 8,
	TAB: 9,
	ENTER: 13,
	SHIFT: 16,
	CTRL: 17,
	ALT: 18,
	PAUSE: 19,
	CAPS_LOCK: 20,
	ESCAPE: 27,
	SPACE: 32,
	PAGE_UP: 33,
	PAGE_DOWN: 34,
	END: 35,
	HOME: 36,
	LEFT_ARROW: 37,
	UP_ARROW: 38,
	RIGHT_ARROW: 39,
	DOWN_ARROW: 40,
	INSERT: 45,
	DELETE: 46,
	KEY_0: 48,
	KEY_1: 49,
	KEY_2: 50,
	KEY_3: 51,
	KEY_4: 52,
	KEY_5: 53,
	KEY_6: 54,
	KEY_7: 55,
	KEY_8: 56,
	KEY_9: 57,
	KEY_A: 65,
	KEY_B: 66,
	KEY_C: 67,
	KEY_D: 68,
	KEY_E: 69,
	KEY_F: 70,
	KEY_G: 71,
	KEY_H: 72,
	KEY_I: 73,
	KEY_J: 74,
	KEY_K: 75,
	KEY_L: 76,
	KEY_M: 77,
	KEY_N: 78,
	KEY_O: 79,
	KEY_P: 80,
	KEY_Q: 81,
	KEY_R: 82,
	KEY_S: 83,
	KEY_T: 84,
	KEY_U: 85,
	KEY_V: 86,
	KEY_W: 87,
	KEY_X: 88,
	KEY_Y: 89,
	KEY_Z: 90,
	LEFT_META: 91,
	RIGHT_META: 92,
	SELECT: 93,
	NUMPAD_0: 96,
	NUMPAD_1: 97,
	NUMPAD_2: 98,
	NUMPAD_3: 99,
	NUMPAD_4: 100,
	NUMPAD_5: 101,
	NUMPAD_6: 102,
	NUMPAD_7: 103,
	NUMPAD_8: 104,
	NUMPAD_9: 105,
	MULTIPLY: 106,
	ADD: 107,
	SUBTRACT: 109,
	DECIMAL: 110,
	DIVIDE: 111,
	F1: 112,
	F2: 113,
	F3: 114,
	F4: 115,
	F5: 116,
	F6: 117,
	F7: 118,
	F8: 119,
	F9: 120,
	F10: 121,
	F11: 122,
	F12: 123,
	NUM_LOCK: 144,
	SCROLL_LOCK: 145,
	SEMICOLON: 186,
	EQUALS: 187,
	COMMA: 188,
	DASH: 189,
	PERIOD: 190,
	FORWARD_SLASH: 191,
	GRAVE_ACCENT: 192,
	OPEN_BRACKET: 219,
	BACK_SLASH: 220,
	CLOSE_BRACKET: 221,
	SINGLE_QUOTE: 222
};

enums.Direction = {
	Up: 1,
	Down: 2,
	Left: 3,
	Right: 4
};

enums.Fonts = {
	Default: "10px 'Press Start 2P'",
	Big: "20px 'Press Start 2P'"
}

enums.GameStates = {
	Idle: 0,
	Game: 1,
	Dead: 2

}

function Graphics (ctx)
{
	var _ctx = ctx;

	_ctx.canvas.width = 500;
	_ctx.canvas.height = 500;

	this.DrawLine = function (p1, p2)
	{
		_ctx.beginPath();
		_ctx.moveTo(p1.X, p1.Y);
		_ctx.lineTo(p2.X, p2.Y);
		_ctx.stroke();
	};

	this.DrawText = function (pos, text, font, color)
	{
		_ctx.font = font;
		_ctx.fillStyle = color;
		_ctx.fillText(text, pos.X, pos.Y);
	};

	this.DrawRectangle = function (rectangle, color)
	{
		_ctx.fillStyle = color;
		_ctx.fillRect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
	};

	this.DrawImage = function (rectangle, image)
	{
		_ctx.drawImage(image, rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
	}

}

// function SoundEffects ()
// {
	// var powerup = [];
	// var die;

	// var initialize = function()
	// {
		// for (var i = 1; i <= 5; i++)
		// {
			// powerup.push(new Audio("soundEffects/Jump" + i + ".wav"));
			// powerup[i-1].volume = 0.5;
		// }

		// die = new Audio("soundEffects/die.wav");
		// die.volume = 0.5;
	// };

	// initialize();

	// this.Play = function ( name )
	// {

		// switch (name)
		// {
			// case 'powerup':
			// {
				// powerup[Helpers.GetRandomValue(0,4)].play();
				// break;
			// }
			// case 'die':
			// {
				// die.play();
				// break;
			// }
		// }
	// }

// }

function Game (display, cellSize)
{
	var _gameState = enums.GameStates.Idle;

	var graphics = new Graphics(display);

	var keyboardState = new KeyboardState();

	// var soundEffects = new SoundEffects();

	var _cellSize = cellSize;

	var snakeController = new SnakeController(_cellSize);
	var appleController = new AppleController(_cellSize);

	var _score = 0;

	var Initialize = function ()
	{
		window.addEventListener(
			'keyup',
			function (event)
			{
				keyboardState.OnKeyUp(event);
			},
			false);

		window.addEventListener(
			'keydown',
			function (event)
			{
				keyboardState.OnKeyDown(event);
			},
			false);

		$('#display').on('touchstart click',
			function(event)
			{
				var pos = getClickPosition(event);

				snakeController.MobileInput(pos);
				if (Helpers.RectanglesIntersects(new Rectangle(pos.X, pos.Y, 1, 1), new Rectangle(140, 140, 221, 221)))
				{
					if (_gameState == enums.GameStates.Dead)
						Reset();

					_gameState = enums.GameStates.Game;
				}
			}
			);

if(document.getElementById("easyButton") != null)
	$('#easyButton').on('touchstart click',
			function(event)
			{
				step = 1/4;
			}
		);

		if(document.getElementById("normalButton") != null)
			$('#normalButton').on('touchstart click',
			function(event)
			{
				step = 1/8;
			}
		);

		if(document.getElementById("hardButton") != null)
			$('#hardButton').on('touchstart click',
			function(event)
			{
				step = 1/16;
			}
		);

		if(document.getElementById("veryHardButton") != null)
			$('#veryHardButton').on('touchstart click',
				function(event)
				{
					step = 1/32;
				}
			);
	};

	Initialize();

	this.Update = function ()
	{
		switch (_gameState)
		{
			case enums.GameStates.Idle:
			{
				if (keyboardState.IsKeyDown(enums.Keys.SPACE))
				{
					Reset();
					_gameState = enums.GameStates.Game;

				}
				break;
			}

			case enums.GameStates.Game :
			{
				snakeController.Update();
				appleController.Update();
				CheckAppleCollision();
				break;
			}

			case enums.GameStates.Dead :
			{
				if (keyboardState.IsKeyDown(enums.Keys.SPACE))
				{
					Reset();
					_gameState = enums.GameStates.Game;

				}
				break;
			}
		}


		if (Constraint.OutOfScreenBounds(snakeController.GetParts()) || Constraint.PartsIntersects(snakeController.GetParts()))
		{
			if ( _gameState != enums.GameStates.Dead )
			// if(soundEffects !=null ) soundEffects.Play('die');

			_gameState = enums.GameStates.Dead;
		}


	};

	var CheckAppleCollision = function ()
	{
		var apples = appleController.GetApples();
		var snakeHead = snakeController.GetParts()[0];

		for (var i = 0; i < apples.length; i++)
		{
			if (Helpers.RectanglesIntersects(apples[i].Rectangle(), snakeHead.Rectangle()))
			{
				snakeController.AddPart();
				apples.splice(i, 1);
				_score++;

				// if(soundEffects !=null ) soundEffects.Play('powerup');

				return;
			}
		}
	};

	this.Input = function ()
	{
		snakeController.Input(keyboardState);
	};

	var Reset = function ()
	{
		snakeController = new SnakeController(_cellSize);
		appleController = new AppleController(_cellSize);

		_score = 0;
	};

	this.Draw = function ()
	{
		switch (_gameState)
		{
			case enums.GameStates.Idle :
			{

				graphics.DrawText(new Point(199.5, 250), "HELLO", enums.Fonts.Big, "#FFFFFF");
				graphics.DrawText(new Point(150, 275), "Press space to play!", enums.Fonts.Default, "#FFFFFF");
				break;
			}

			case enums.GameStates.Game :
			{
				snakeController.Draw(graphics);
				appleController.Draw(graphics);

				graphics.DrawText(new Point(20, 30), _score, enums.Fonts.Default, "#FFFFFF");
				break;
			}

			case enums.GameStates.Dead :
			{
				snakeController.Draw(graphics);
				appleController.Draw(graphics);

				graphics.DrawText(new Point(20, 30), _score, enums.Fonts.Default, "#FFFFFF");
				graphics.DrawText(new Point(61, 250), "You are died... Press space to reborn!", enums.Fonts.Default, "#FFFFFF");
				break;
			}

		}

	};
}

function SnakeController(cellSize)
{


	var _snakeParts = [];

	var _moveDirection = enums.Direction.Down;
	var _moveSpeed = cellSize;

	this.AddPart = function ()
	{
		var lastPart = _snakeParts[_snakeParts.length-1];

		_snakeParts.push( new SnakePart(lastPart.PrevRectangle.X, lastPart.PrevRectangle.Y, _moveSpeed, enums.Colors.Body) );
	};

	var Initialize = function ()
	{
		_snakeParts.push( new SnakePart(2*_moveSpeed,2*_moveSpeed, _moveSpeed, enums.Colors.Head) );
	};

	Initialize();

	this.Update = function ()
	{
		Movement();
	};

	this.Input = function (keyboardState)
	{
		if (keyboardState.IsKeyDown(enums.Keys.KEY_A) || keyboardState.IsKeyDown(enums.Keys.LEFT_ARROW))
		{
			if ( _moveDirection != enums.Direction.Right)
			_moveDirection = enums.Direction.Left;

		}

		if (keyboardState.IsKeyDown(enums.Keys.KEY_D) || keyboardState.IsKeyDown(enums.Keys.RIGHT_ARROW))
		{
			if (_moveDirection != enums.Direction.Left)
				_moveDirection = enums.Direction.Right;
		}
		if (keyboardState.IsKeyDown(enums.Keys.KEY_W) || keyboardState.IsKeyDown(enums.Keys.UP_ARROW))
		{
			if ( _moveDirection != enums.Direction.Down)
				_moveDirection = enums.Direction.Up;
		}

		if (keyboardState.IsKeyDown(enums.Keys.KEY_S) || keyboardState.IsKeyDown(enums.Keys.DOWN_ARROW))
		{
			if (_moveDirection != enums.Direction.Up)
				_moveDirection = enums.Direction.Down;
		}
	};

	this.MobileInput = function (clickPos)
	{
		var __clickRectangle = new Rectangle(clickPos.X, clickPos.Y, 1, 1);

		var __top = new Rectangle(140, 0, 220, 140);
		var __bottom = new Rectangle(140, 360, 220, 140);
		var __left = new Rectangle(0, 140, 140, 220);
		var __right = new Rectangle(340, 140, 140, 220);

		if(Helpers.RectanglesIntersects(__clickRectangle, __left))
		{
			_moveDirection = enums.Direction.Left;
		}

		if(Helpers.RectanglesIntersects(__clickRectangle, __right))
		{
			_moveDirection = enums.Direction.Right;
		}

		if(Helpers.RectanglesIntersects(__clickRectangle, __top))
		{
			_moveDirection = enums.Direction.Up;
		}

		if(Helpers.RectanglesIntersects(__clickRectangle, __bottom))
		{
			_moveDirection = enums.Direction.Down;
		}


	};

	var Movement = function ()
	{
		_snakeParts[0].PrevRectangle = _snakeParts[0].GetNewRect();

		switch (_moveDirection)
		{
			case enums.Direction.Up:
				_snakeParts[0].Rectangle().Y -= _moveSpeed;
				break;
			case enums.Direction.Down:
				_snakeParts[0].Rectangle().Y += _moveSpeed;
				break;
			case enums.Direction.Left:
				_snakeParts[0].Rectangle().X -= _moveSpeed;
				break;
			case enums.Direction.Right:
				_snakeParts[0].Rectangle().X += _moveSpeed;
				break;
		}

		for	(var i = 1; i < _snakeParts.length; i++)
		{
			_snakeParts[i].PrevRectangle = _snakeParts[i].GetNewRect();
			_snakeParts[i].Rectangle().X = _snakeParts[i - 1].PrevRectangle.X ;
			_snakeParts[i].Rectangle().Y = _snakeParts[i - 1].PrevRectangle.Y ;

			debugInfo = i;
		}

	};


	this.GetParts = function ()
	{
		return _snakeParts;
	};

	this.Draw = function (graphics)
	{
		for (var i = 0; i < _snakeParts.length; i++)
			_snakeParts[i].Draw(graphics);

	};

}

function SnakePart (x, y, size, color)
{
	var _rectangle = new Rectangle(x, y, size, size);
	var _prevRectangle = new Rectangle(x, y, size, size);

	var _color = color;

	this.PrevRectangle = function()
	{
		return _prevRectangle;
	};

	this.GetNewRect = function ()
	{
		return new Rectangle(_rectangle.X, _rectangle.Y, _rectangle.Width, _rectangle.Height);
	};

	this.Rectangle = function () {
		return _rectangle;
	};

	this.Draw = function (graphics)
	{
		graphics.DrawRectangle(_rectangle, _color);
	}

}

function AppleController (cellSize)
{
	var _apples = [];
	var _framesSkipped = 0;
	var _framesSkipCount = 10;
	var _size = cellSize;


	this.GetApples = function ()
	{
		return _apples;
	};

	this.Update = function ()
	{
		_framesSkipped++;

		var rowColumsCount = 500 / cellSize;

		if (_framesSkipped == _framesSkipCount)
		{
			_apples.push(new Apple(
					Helpers.GetRandomValue(0, rowColumsCount)* _size,
					Helpers.GetRandomValue(0, rowColumsCount)* _size,
					_size)
			);

			_framesSkipped = 0;
		}
	};

	this.Draw = function (graphics)
	{
		for (var i = 0; i < _apples.length; i++)
			_apples[i].Draw(graphics);

	};
}

function Apple (x, y, size)
{

	var _color = enums.Colors.Apple;

	var _rectangle = new Rectangle(x, y, size, size);

	this.Rectangle = function () {
		return _rectangle;
	};

	this.Draw = function (graphics)
	{
		graphics.DrawRectangle(_rectangle, enums.Colors.Apple);

	}
}

var Constraint = {};

/**
 * @return {boolean}
 */
Constraint.OutOfScreenBounds = function (snakeParts)
{
	return !Helpers.RectanglesIntersects(SCREEN_BOUNDS, snakeParts[0].GetNewRect());
};

/**
 * @return {boolean}
 */
Constraint.PartsIntersects = function (snakeParts)
{
	for (var i = 1; i < snakeParts.length; i++)
	{
		if (Helpers.RectanglesIntersects(snakeParts[i].GetNewRect(), snakeParts[0].GetNewRect()))
		{
			return true;
		}
	}

	return false;
};


var Helpers = {};

Helpers.Create2DArray = function (rows)
	{
		var arr = [];

		for (var i = 0; i < rows; i++)
		{
			arr[i] = [];
		}

		return arr;
	};


Helpers.GetRandomValue = function (min, max)
{
	return Math.floor(Math.random() * max) + min;
};

Helpers.RectanglesIntersects = function (rect1, rect2)
{
	return !!(rect1.X < rect2.X + rect2.Width &&
	rect1.X + rect1.Width > rect2.X &&
	rect1.Y < rect2.Y + rect2.Height &&
	rect1.Height + rect1.Y > rect2.Y);
};

var CellSize = 50;

var NewGame = function ()
	{
		var displayElement = document.getElementById("display");
		var display = displayElement.getContext("2d");
		var now,
			dt = 0,
			last = timestamp();
			step = 1/8;
		var game = new Game(display, CellSize);


		function Frame() {
			now = timestamp();
			dt = dt + Math.min(1, (now - last) / 1000);
			while(dt > step) {
				dt = dt - step;
				Update();
			}
			last = now;
			game.Input();
			Draw();
			requestAnimationFrame(Frame);
		}

		function Update()
		{
			game.Update();
		}

		function Draw ()
		{
			display.fillStyle = "#000000"
			display.fillRect(0, 0, 500, 500);
			game.Draw();
		}

		requestAnimationFrame(Frame);

	};

NewGame();
